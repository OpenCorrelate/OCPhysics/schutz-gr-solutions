# Notes and Exercise Solutions to *A First Course in General Relativity* by Bernard Schutz

Gonna learn some [GR](https://en.wikipedia.org/wiki/General_relativity). This ought to end well.

Primary textbook is [A First Course in General Relativity](https://www.goodreads.com/book/show/53520.A_First_Course_in_General_Relativity) by [Bernard F. Schutz](https://en.wikipedia.org/wiki/Bernard_F._Schutz). Will also refer to [Sean Carroll](http://www.preposterousuniverse.com/blog/)'s [relativity notes](https://www.preposterousuniverse.com/grnotes/) among other gems.

Using [Gitlab](https://gitlab.com) cuz [Github](https://github.com) don't do TeX unless I use GH pages, and fuck that shit.

## Chapters

1. [Special Relativity](chapters/chapter01/README.md) ([solutions](chapters/chapter01/exercises/README.md))

## AUTHORS

Prez Cannady ([gitlab](https://gitlab.com/revprez))
