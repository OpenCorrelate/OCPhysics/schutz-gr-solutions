# Special Relativity

## Directions

* [Table of Contents](../../README.md)
* Solutions availables [here](exercises/README.md).
* Text available [here](https://www.amazon.com/First-Course-General-Relativity/dp/0521887054)


## Fundamental principles of special relativity (SR) theory

* Start by gaining an algebraic and geometric intuition for relativity.
* Deduce special relativity from two postulates:
    - [Galileo](https://en.wikipedia.org/wiki/Galilean_invariance): **Unaccelerated** observers can only measure velocities with respect to their own motion. An observer traveling at velocity $`v(t)`$ will measure an event traveling at $`v'(t)`$ as:

        ```math
        v(t) \rightarrow v'(t) = v(t) + V
        ```

        where $`V`$ is some constant velocity. Note that Newton's laws are also invariant under Galilean addition, for example:

        ```math
        F = ma = m \frac{dv}{dt}
        ```
        ```math
        a' - \frac{dv'}{dt} = \frac{d(v - V)}{dt} = \frac{dv}{dt} = a
        ```


    - **Einstein**: The speed of light is invariant in all frames of reference. That is regardless of motion, the observed speed of light remains the same  (~3e8 m/s). This has profound implications for measuring velocities in inertial frames of reference.




## Additional reading

1. [Lorentz transformation](https://en.wikipedia.org/wiki/Lorentz_transformation), [Wikipedia](https://en.wikipedia.org)
